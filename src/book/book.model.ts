import { DataTypes, Model } from 'sequelize';
import { AuthorModel } from '../author/author.model';
import { BorrowModel } from '../borrow/borrow.model';
import { IDto, IModel } from '../common/abstract.model';
import { sequelize } from '../common/database.config';
import { GenreModel } from '../genre/genre.model';
import { UserModel } from '../user/user.model';

export interface IBook extends IModel {
  title: string;
  genre_id;
  author_id;
  genre: GenreModel;
  author: AuthorModel;
  users: UserModel[];
  borrows: BorrowModel[];
  createdAt;
}

export interface IBookDto extends IDto {
  title: string;
  genre_id;
  author_id;
  genre: GenreModel;
  author: AuthorModel;
  users: UserModel[];
  borrows: BorrowModel[];
  createdAt;
}

const attributes = {
  title: {
    type: DataTypes.STRING,
    allowNull: false
  },
  genre_id: {
    type: DataTypes.INTEGER.UNSIGNED,
    references: {
      model: 'genre',
      key: 'id',
      as: 'genre_id'
    }
  },
  author_id: {
    type: DataTypes.INTEGER.UNSIGNED,
    references: {
      model: 'author',
      key: 'id',
      as: 'author_id'
    }
  },
};

const options = {
  sequelize,
  modelName: 'book'
};

export class BookModel extends Model implements IBook {
  id: number;
  title: string;
  genre_id;
  author_id;
  genre: GenreModel
  author: AuthorModel;
  users: UserModel[];
  borrows: BorrowModel[];
  createdAt;
}
BookModel.init(attributes, options);
