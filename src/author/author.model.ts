import { DataTypes, Model } from 'sequelize';
import { BookModel } from '../book/book.model';
import { IDto, IModel } from '../common/abstract.model';
import { sequelize } from '../common/database.config';

export interface IAuthor extends IModel {
  firstName: string,
  lastName: string,
  books: BookModel[]
}

export interface IAuthorDto extends IDto {
  firstName: string,
  lastName: string,
  books: BookModel[]
}

const attributes = {
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: false
  }
};

const options = {
  sequelize,
  modelName: 'author'
};

export class AuthorModel extends Model implements IAuthor {
  id: number;
  firstName: string;
  lastName: string;
  books: BookModel[]
}
AuthorModel.init(attributes, options);
