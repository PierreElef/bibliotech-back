import express from 'express';
import { authRouter } from './auth/auth.router';
import { bookRouter } from './book/book.router';
import { genreRouter } from './genre/genre.router';
import { authorRouter } from './author/author.router';
import { userRouter } from './user/user.router';
import { borrowRouter } from './borrow/borrow.router';
import { routeNotFoundMiddleware } from './common/route-not-found.middleware';

export const router = express.Router();
router.use('/auth', authRouter);
router.use('/genres', genreRouter);
router.use('/authors', authorRouter);
router.use('/books', bookRouter);
router.use('/borrows', borrowRouter);
router.use('/users', userRouter);
router.use(routeNotFoundMiddleware);