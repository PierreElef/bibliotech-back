import { AbstractRepository } from '../common/abstract.repository';
import { AuthorModel, IAuthor } from './author.model';

class AuthorRepository extends AbstractRepository<IAuthor> {
  protected modelClass = AuthorModel;
}

export const authorRepository = new AuthorRepository();
