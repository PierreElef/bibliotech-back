import { Sequelize } from 'sequelize';

export const sequelize = new Sequelize(
  'bibliotech_bdd', 
  '232540_user', 
  'Ynov2021', {
    host: 'mysql-bibliotech.alwaysdata.net',
    dialect: 'mysql'
  }
);

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
    return sequelize.sync();
  })
  .then(() => console.log('Database synchronized'))
  .catch(err => console.error('Database error:', err));
