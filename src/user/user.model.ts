import { DataTypes, Model } from 'sequelize';
import { BookModel } from '../book/book.model';
import { IModel } from '../common/abstract.model';
import { sequelize } from '../common/database.config';

export enum UserRole {
	member = 'MEMBER',
	admin = 'ADMIN'
}

export interface IUser extends IModel {
	firstName: string;
	lastName: string;
	email: string;
	role: UserRole;
	password: string;
	books: BookModel[];
}

export interface IUserDto extends IModel {
	firstName: string;
	lastName: string;
	email: string;
	role: UserRole;
	password: string;
	books: BookModel[];
}

const attributes = {
    firstName: {
		type: DataTypes.STRING,
		allowNull: false
	},
	lastName: {
		type: DataTypes.STRING,
		allowNull: false
	},
	email: {
		type: DataTypes.STRING,
		allowNull: false
	},
	role: {
		type: DataTypes.STRING,
		allowNull: false
	},
	password: {
		type: DataTypes.STRING,
		allowNull: false
	}
};

const options = {
	sequelize,
	modelName: 'user'
};

export class UserModel extends Model implements IUser {
	id: number;
    firstName: string;
	lastName: string;
	email: string;
	role: UserRole;
	password: string;
	books: BookModel[];
}
UserModel.init(attributes, options);
