import { AbstractMapper } from '../common/abstract.mapper';
import { IBorrow, IBorrowDto } from './borrow.model';

export class BorrowMapper extends AbstractMapper<IBorrow, IBorrowDto> {

  modelToDto(model: IBorrow): IBorrowDto {
    return {
      id: model.id,
      date_begin: model.date_begin,
      date_end: model.date_end,
      book_id: model.book_id,
      user_id: model.user_id,
      user: model.user,
      book: model.book,
    };
  }

  dtoToModel(dto: IBorrowDto): IBorrow {
    return {
      id: dto.id,
      date_begin: dto.date_begin,
      date_end: dto.date_end,
      book_id: dto.book_id,
      user_id: dto.user_id,
      user: dto.user,
      book: dto.book
    };
  }
}

export const borrowMapper = new BorrowMapper();
