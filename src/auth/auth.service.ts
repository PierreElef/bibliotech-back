import { ErrorType } from '../common/error/error.model';
import { generateToken } from '../common/token.service';
import { userRepository } from '../user/user.repository';
import { authMeMapper } from './auth.mapper';
import { IAuthCredentials, IAuthMeDto, IAuthToken } from './auth.model';

class AuthService {

  login(credentials: IAuthCredentials): Promise<IAuthToken> {
    return userRepository.findByEmailAndPassword(credentials.login, credentials.pwd)
      .then(user => generateToken({
        id: user.id,
        email: user.email,
        role: user.role
      }))
      .then(token => ({ token }))
      .catch(error => {
        console.error(error);
        return Promise.reject({ type: ErrorType.invalidCredentials });
      })
  }

  getUser(userId: number): Promise<IAuthMeDto> {
    return userRepository.get(userId)
      .then(model => authMeMapper.modelToDto(model))
  }
}

export const authService = new AuthService();
