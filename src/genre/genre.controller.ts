import { NextFunction, Request, Response } from 'express';
import { AbstractController } from '../common/abstract.controller';
import { AuthenticatedRequest } from '../common/authentication.middleware';
import { IGenre, IGenreDto } from './genre.model';
import { genreService } from './genre.service';

class GenreController extends AbstractController<IGenre, IGenreDto> {
  protected service = genreService;

  findAll(request: AuthenticatedRequest, response: Response, next: NextFunction) {
    super.findAll(request, response, next);
  }
}

export const genreController = new GenreController();
