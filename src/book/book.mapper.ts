import { AbstractMapper } from '../common/abstract.mapper';
import { IBook, IBookDto } from './book.model';

export class BookMapper extends AbstractMapper<IBook, IBookDto> {

  modelToDto(model: IBook): IBookDto {
    return {
      id: model.id,
      title: model.title,
      genre_id: model.genre_id,
      author_id: model.author_id,
      genre: model.genre,
      author: model.author,
      users: model.users,
      borrows: model.borrows,
      createdAt: model.createdAt,
    };
  }

  dtoToModel(dto: IBookDto): IBook {
    return {
      id: dto.id,
      title: dto.title,
      genre_id: dto.genre_id,
      author_id: dto.author_id,
      genre: dto.genre,
      author: dto.author,
      users: dto.users,
      borrows: dto.borrows,
      createdAt: dto.createdAt
    };
  }
}

export const bookMapper = new BookMapper();
