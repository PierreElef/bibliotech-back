import { NextFunction, Request, Response } from 'express';
import { AbstractController } from '../common/abstract.controller';
import { AuthenticatedRequest } from '../common/authentication.middleware';
import { IUser, IUserDto } from './user.model';
import { userService } from './user.service';

class UserController extends AbstractController<IUser, IUserDto> {
  protected service = userService;

  findAll(request: AuthenticatedRequest, response: Response, next: NextFunction) {
    super.findAll(request, response, next);
  }
}

export const userController = new UserController();
