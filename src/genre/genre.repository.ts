import { AbstractRepository } from '../common/abstract.repository';
import { GenreModel, IGenre } from './genre.model';

class GenreRepository extends AbstractRepository<IGenre> {
  protected modelClass = GenreModel;
}

export const genreRepository = new GenreRepository();
