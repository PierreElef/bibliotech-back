import { AbstractRouter } from '../common/abstract.router';
import { bookController } from './book.controller';
import { IBook, IBookDto } from './book.model';

class BookRouter extends AbstractRouter<IBook, IBookDto> {
  protected controller = bookController;
}

export const bookRouter = new BookRouter().router;
