import { AbstractMapper } from '../common/abstract.mapper';
import { IGenre, IGenreDto } from './genre.model';

export class GenreMapper extends AbstractMapper<IGenre, IGenreDto> {

  modelToDto(model: IGenre): IGenreDto {
    return {
      id: model.id,
      libel: model.libel,
      books: model.books
    };
  }

  dtoToModel(dto: IGenreDto): IGenre {
    return {
      id: dto.id,
      libel: dto.libel,
      books: dto.books
    };
  }
}

export const genreMapper = new GenreMapper();
