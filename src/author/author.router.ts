import { AbstractRouter } from '../common/abstract.router';
import { authorController } from './author.controller';
import { IAuthor, IAuthorDto } from './author.model';

class AuthorRouter extends AbstractRouter<IAuthor, IAuthorDto> {
  protected controller = authorController;
}

export const authorRouter = new AuthorRouter().router;
