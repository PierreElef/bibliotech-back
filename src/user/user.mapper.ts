import { AbstractMapper } from '../common/abstract.mapper';
import { IUser, IUserDto } from './user.model';

export class UserMapper extends AbstractMapper<IUser, IUserDto> {

  modelToDto(model: IUser): IUserDto {
    return {
      id: model.id,
      firstName: model.firstName,
      lastName: model.lastName,
      email: model.email,
      role: model.role,
      password: model.password,
      books: model.books
    };
  }

  dtoToModel(dto: IUserDto): IUser {
    return {
      id: dto.id,
      firstName: dto.firstName,
      lastName: dto.lastName,
      email: dto.email,
      role: dto.role,
      password: dto.password,
      books: dto.books
    };
  }
}

export const userMapper = new UserMapper();
