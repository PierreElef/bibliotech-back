import { NextFunction, Request, Response } from 'express';
import { AbstractController } from '../common/abstract.controller';
import { AuthenticatedRequest } from '../common/authentication.middleware';
import { IAuthor, IAuthorDto } from './author.model';
import { authorService } from './author.service';

class AuthorController extends AbstractController<IAuthor, IAuthorDto> {
  protected service = authorService;

  findAll(request: AuthenticatedRequest, response: Response, next: NextFunction) {
    super.findAll(request, response, next);
  }
}

export const authorController = new AuthorController();
