import { BookModel } from '../book/book.model';
import { IDto } from '../common/abstract.model';
import { UserRole } from '../user/user.model';

export interface IAuthCredentials {
  login: string;
  pwd: string;
}

export interface IAuthToken {
  token: string;
}

export interface IAuthMeDto extends IDto {
  firstName: string;
  lastName: string;
  email: string;
  role: UserRole;
  books: BookModel[];
}
