import { AbstractMapper } from '../common/abstract.mapper';
import { IUser } from '../user/user.model';
import { IAuthMeDto } from './auth.model';

class AuthMeMapper extends AbstractMapper<IUser, IAuthMeDto> {
  dtoToModel(dto: IAuthMeDto): IUser {
    return undefined;
  }

  modelToDto(model: IUser): IAuthMeDto {
    return {
      id: model.id,
      firstName: model.firstName,
      lastName: model.lastName,
      email: model.email,
      role: model.role,
      books: model.books
    };
  }
}

export const authMeMapper = new AuthMeMapper();
