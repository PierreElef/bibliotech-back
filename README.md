# Bibliotech - Back

## Couches Techniques
Pour chaque module, il y a :
- un router qui donne les différentes routes du module, protège les routes s'il y a un middleware et appelle le controller
- un controller qui appelle les méthodes du service et envoie la réponse
- un service qui fait le lien entre le repository et le mapper
- un repository qui fait les appels à la base de données
- un mapper qui met en forme les données en fonction du model
- un model qui définit le format des données du module

## Système d'authentification
Une route 'login' qui attend un email et un password renvoie le token si l'email est reconnu et le mot de passe correspond à celui de la base de données.
Une route 'me' permet d'identifier l'utilisateur qui est connecté
Les autres routes de l'API, protégé par le middleware d'authentification attend qu'il y ait un token dans le header de la requête pour renvoyer sa réponse, sinon il envoie une erreur 401.

## Setup

### Base de données

La base de données est hébergée sur always data

Dans le cas où une instanciation de la base de données est nécessaire,

Le script bibliotech.sql permet de générer la structure de la base de données

Le script dumpBibliotech.sql permet d'ajouter des données de tests

### Step by step
1. NPM module (project descriptor)
```
npm init -y
```

2. Nodemon (application auto reload)
```
npm install --save-dev nodemon
```

3. Typescript (JavaScript superset language)
```
npm install --save-dev typescript ts-node @types/node
```

4. Moment (date util library)
```
npm install moment
```

5. Express (server framework)
```
npm install express
npm install --save-dev @types/express
```

6. Cors
```
npm i --save-dev @types/cors
```


### Developer
```
npm install
```
