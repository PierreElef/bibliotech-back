import { DataTypes, Model } from 'sequelize';
import { BookModel } from '../book/book.model';
import { IDto, IModel } from '../common/abstract.model';
import { sequelize } from '../common/database.config';
import { UserModel } from '../user/user.model';

export interface IBorrow extends IModel {
  date_begin: Date;
  date_end: Date;
  book_id:number;
  user_id:number;
  user: UserModel;
  book: BookModel;
}

export interface IBorrowDto extends IDto {
  date_begin: Date;
  date_end: Date;
  book_id:number;
  user_id:number;
  user: UserModel;
  book: BookModel;
}

const attributes = {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  date_begin: {
    type: DataTypes.DATE,
    allowNull: true
  },
  date_end: {
    type: DataTypes.DATE,
    allowNull: true
  },
  user_id: {
    type: DataTypes.INTEGER.UNSIGNED,
    references: {
      model: 'user',
      key: 'id',
      as: 'user_id'
    }
  },
  book_id: {
    type: DataTypes.INTEGER.UNSIGNED,
    references: {
      model: 'book',
      key: 'id',
      as: 'book_id'
    }
  }
};

const options = {
  sequelize,
  modelName: 'borrow'
};

export class BorrowModel extends Model implements IBorrow {
  id: number;
  date_begin: Date;
  date_end: Date;
  book_id: number;
  user_id: number;
  user: UserModel;
  book: BookModel;
}
BorrowModel.init(attributes, options);
