import { AuthorModel } from "../author/author.model";
import { BookModel } from "../book/book.model";
import { BorrowModel } from "../borrow/borrow.model";
import { GenreModel } from "../genre/genre.model";
import { UserModel } from "../user/user.model";


// n-1 association between Book and Genre

BookModel.belongsTo(GenreModel, { foreignKey: 'genre_id', targetKey: 'id', as:'genre' });
GenreModel.hasMany(BookModel, {sourceKey: 'id', foreignKey: 'genre_id', as: 'books'});

// n-1 association between Book and Author

BookModel.belongsTo(AuthorModel, { foreignKey: 'author_id', as: 'author' });
AuthorModel.hasMany(BookModel, { sourceKey: 'id', foreignKey: 'author_id', as: 'books' });

// n-n association between User and Book

BookModel.belongsToMany(UserModel, {through: BorrowModel, foreignKey: 'book_id', otherKey: 'user_id' });
UserModel.belongsToMany(BookModel, {through: BorrowModel, foreignKey: 'user_id', otherKey: 'book_id' });

// n-1 association between Book and Borrows

BorrowModel.belongsTo(BookModel, { foreignKey: 'book_id', as: 'book' });
BookModel.hasMany(BorrowModel, { sourceKey: 'id', foreignKey: 'book_id', as: 'borrows' });

// n-1 association between User and Borrows

BorrowModel.belongsTo(UserModel, { foreignKey: 'user_id', as: 'user' });
UserModel.hasMany(BorrowModel, { sourceKey: 'id', foreignKey: 'user_id', as: 'borrows' });