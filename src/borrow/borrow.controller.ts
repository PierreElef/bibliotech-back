import { NextFunction, Request, Response } from 'express';
import { AbstractController } from '../common/abstract.controller';
import { AuthenticatedRequest } from '../common/authentication.middleware';
import { IBorrow, IBorrowDto } from './borrow.model';
import { borrowService } from './borrow.service';

class BorrowController extends AbstractController<IBorrow, IBorrowDto> {
  protected service = borrowService;

  findAll(request: AuthenticatedRequest, response: Response, next: NextFunction) {
    super.findAll(request, response, next);
  }
}

export const borrowController = new BorrowController();
