import { Request } from 'express';

export interface IPaginationParams {
  size: number;
  page: number;
}

const extractQueryParamAsInteger = (param: string | string[] | any): number => {
  return parseInt((Array.isArray(param) ? param[0] : param) as string, 10);
}

const PAGINATION_MAXIMUM_SIZE = 100;
const PAGINATION_DEFAULT_SIZE = 20;
const PAGINATION_DEFAULT_PAGE = 0;

export const extractPaginationParams = (request: Request): IPaginationParams => {
  const size = Math.min(extractQueryParamAsInteger(request.query.size), PAGINATION_MAXIMUM_SIZE) || PAGINATION_DEFAULT_SIZE;
  const page = extractQueryParamAsInteger(request.query.page) || PAGINATION_DEFAULT_PAGE;
  return { size, page };
}
