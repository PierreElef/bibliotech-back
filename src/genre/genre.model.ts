import { DataTypes, Model } from 'sequelize';
import { BookModel } from '../book/book.model';
import { IDto, IModel } from '../common/abstract.model';
import { sequelize } from '../common/database.config';

export interface IGenre extends IModel {
  libel: string,
  books: BookModel[]
}

export interface IGenreDto extends IDto {
  libel: string;
  books: BookModel[]
}

const attributes = {
  id: {
    type: DataTypes.INTEGER.UNSIGNED,
    autoIncrement: true,
    primaryKey: true,
  },
  libel: {
    type: DataTypes.STRING,
    allowNull: false
  },
};

const options = {
  sequelize,
  modelName: 'genre'
};

export class GenreModel extends Model implements IGenre {
  id: number;
  libel: string;
  books: BookModel[]
}
GenreModel.init(attributes, options);
