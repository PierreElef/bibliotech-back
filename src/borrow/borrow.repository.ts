import { AbstractRepository } from '../common/abstract.repository';
import { BorrowModel, IBorrow } from './borrow.model';

class BorrowRepository extends AbstractRepository<IBorrow> {
  protected modelClass = BorrowModel;
}

export const borrowRepository = new BorrowRepository();
