import { AbstractRouter } from '../common/abstract.router';
import { genreController } from './genre.controller';
import { IGenre, IGenreDto } from './genre.model';

class GenreRouter extends AbstractRouter<IGenre, IGenreDto> {
  protected controller = genreController;
}

export const genreRouter = new GenreRouter().router;
