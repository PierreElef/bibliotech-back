--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`id`, `firstName`, `lastName`, `createdAt`, `updatedAt`) VALUES
(1, 'Victor', 'Hugo', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Paul', 'Verlaine', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'J. K.', 'Rowling', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'J. R. R.', 'Tolkien', '0000-00-00 00:00:00', '0000-00-00 00:00:00');


--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `libel`, `createdAt`, `updatedAt`) VALUES
(1, 'Drame', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Fantastique', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Poésie', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Essai', '0000-00-00 00:00:00', '0000-00-00 00:00:00');


--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstName`, `lastName`, `email`, `role`, `password`, `createdAt`, `updatedAt`) VALUES
(1, 'Irma', 'Pince', 'irma.pince@poudlard.com', 'ADMIN', 'poudlard', '2021-04-01 00:00:00', '0000-00-00 00:00:00'),
(2, 'Hermione', 'Granger', 'hermione.granger@poudlard.com', 'USER', 'poudlard', '2021-04-01 00:00:00', '0000-00-00 00:00:00'),
(3, 'Harry', 'Potter', 'harry.potter@poudlard.com', 'USER', 'poudlard', '0000-00-00 00:00:00', '0000-00-00 00:00:00');


--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `title`, `genre_id`, `author_id`, `createdAt`, `updatedAt`) VALUES
(1, 'test', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Harry Potter à l\'école des sorciers', 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Harry Potter et la Chambre des secrets', 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Harry Potter et le Prisonnier d\'Azkaban', 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Harry Potter et la Coupe de feu', 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Harry Potter et l\'Ordre du Phénix', 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Harry Potter et le Prince de sang-mêlé', 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Harry Potter et les Reliques de la Mort', 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Le Hobbit', 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'La Communauté de l’anneau', 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Les Deux Tours', 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Le retour du Roi', 2, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Le Bossu de Notre-Dame', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Poèmes saturniens', 3, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

INSERT INTO `borrows` (`id`, `date_begin`, `date_end`, `user_id`, `book_id`, `createdAt`, `updatedAt`) VALUES 
(1, '2021-04-28 00:00:00', '2021-05-12 00:00:00', 1, 6, '2021-04-22 00:00:00', '2021-04-22 00:00:00');
