import { AbstractService } from '../common/abstract.service';
import { genreMapper } from './genre.mapper';
import { IGenre, IGenreDto } from './genre.model';
import { genreRepository } from './genre.repository';

class GenreService extends AbstractService<IGenre, IGenreDto> {
    
  protected repository = genreRepository;
  protected mapper = genreMapper;

}

export const genreService = new GenreService();
