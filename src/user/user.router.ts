import { AbstractRouter } from '../common/abstract.router';
import { userController } from './user.controller';
import { IUser, IUserDto } from './user.model';

class UserRouter extends AbstractRouter<IUser, IUserDto> {
  protected controller = userController;
}

export const userRouter = new UserRouter().router;
