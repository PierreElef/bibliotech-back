import { Model, ModelStatic } from 'sequelize';
import { IModel } from './abstract.model';
import { PrimaryKeyError } from './error/repository-error.model';
import { IPaginationParams } from './pagination.service';

export abstract class AbstractRepository<M extends IModel> {
  
  protected abstract modelClass: ModelStatic<Model<M, M>> & typeof Model;

  findAll(paginationParams: IPaginationParams): Promise<M[]> {
    return this.modelClass.findAll({
      offset: paginationParams.page * paginationParams.size,
      limit: paginationParams.size,
      include: { all: true, nested: true }
    }) as unknown as Promise<M[]>;
  }

  get(id: number): Promise<M> {
    return this.modelClass.findByPk(id, {include: { all: true, nested: true }})
    .then((model) => {
      if (model) {
        return model;
      } else {
        throw new PrimaryKeyError();
      }
    }) as unknown as Promise<M>;
  }

  create(model: M): Promise<M> {
    return this.modelClass.create(model) as unknown as Promise<M>;
  }

  update(id: number, model: M): Promise<M> {
    return this.modelClass.update(model, { where: { id } })
      .then(([affectedRowsCount]) => {
        if (affectedRowsCount) {
          return model;
        } else {
          throw new PrimaryKeyError();
        }
      });
  }

  remove(id: number): Promise<void> {
    return this.modelClass.destroy({ where: { id } })
      .then((affectedRowsCount) => {
        if (!affectedRowsCount) {
          throw new PrimaryKeyError();
        }
      });
  }
}
