import { AbstractService } from '../common/abstract.service';
import { borrowMapper } from './borrow.mapper';
import { IBorrow, IBorrowDto } from './borrow.model';
import { borrowRepository } from './borrow.repository';

class BorrowService extends AbstractService<IBorrow, IBorrowDto> {
    
  protected repository = borrowRepository;
  protected mapper = borrowMapper;

}

export const borrowService = new BorrowService();
