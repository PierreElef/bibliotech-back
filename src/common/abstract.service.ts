import { AbstractMapper } from './abstract.mapper';
import { IDto, IModel } from './abstract.model';
import { AbstractRepository } from './abstract.repository';
import { itemErrorHandler } from './error/error.mapper';
import { IPaginationParams } from './pagination.service';

export abstract class AbstractService<M extends IModel, D extends IDto> {
  protected abstract repository: AbstractRepository<M>;
  protected abstract mapper: AbstractMapper<M, D>

  findAll(paginationParams: IPaginationParams): Promise<D[]> {
    return this.repository.findAll(paginationParams)
      .then(models => this.mapper.modelsToDtos(models));
  }

  get(id: number): Promise<D> {
    return this.repository.get(id)
      .then(model => this.mapper.modelToDto(model))
      .catch(itemErrorHandler(id));
  }

  create(dto: D): Promise<D> {
    const character = this.mapper.dtoToModel(dto);
    return this.repository.create(character)
      .then(model => this.mapper.modelToDto(model));
  }

  update(id: number, dto: D): Promise<D> {
    const character = this.mapper.dtoToModel(dto);
    return this.repository.update(id, character)
      .then(model => this.mapper.modelToDto(model))
      .catch(itemErrorHandler(id));
  }

  remove(id: number): Promise<void> {
    return this.repository.remove(id)
      .catch(itemErrorHandler(id));
  }
} 
