import { AbstractMapper } from '../common/abstract.mapper';
import { IAuthor, IAuthorDto } from './author.model';

export class AuthorMapper extends AbstractMapper<IAuthor, IAuthorDto> {

  modelToDto(model: IAuthor): IAuthorDto {
    return {
      id: model.id,
      firstName: model.firstName,
      lastName: model.lastName,
      books: model.books
    };
  }

  dtoToModel(dto: IAuthorDto): IAuthor {
    return {
      id: dto.id,
      firstName: dto.firstName,
      lastName: dto.lastName,
      books: dto.books
    };
  }
}

export const authorMapper = new AuthorMapper();
