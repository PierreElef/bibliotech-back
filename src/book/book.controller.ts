import { NextFunction, Request, Response } from 'express';
import { AbstractController } from '../common/abstract.controller';
import { AuthenticatedRequest } from '../common/authentication.middleware';
import { IBook, IBookDto } from './book.model';
import { bookService } from './book.service';

class BookController extends AbstractController<IBook, IBookDto> {
  protected service = bookService;

  findAll(request: AuthenticatedRequest, response: Response, next: NextFunction) {
    super.findAll(request, response, next);
  }
}

export const bookController = new BookController();
