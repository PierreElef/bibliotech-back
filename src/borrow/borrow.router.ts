import { AbstractRouter } from '../common/abstract.router';
import { borrowController } from './borrow.controller';
import { IBorrow, IBorrowDto } from './borrow.model';

class BorrowRouter extends AbstractRouter<IBorrow, IBorrowDto> {
  protected controller = borrowController;
}

export const borrowRouter = new BorrowRouter().router;
