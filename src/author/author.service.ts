import { AbstractService } from '../common/abstract.service';
import { authorMapper } from './author.mapper';
import { IAuthor, IAuthorDto } from './author.model';
import { authorRepository } from './author.repository';

class AuthorService extends AbstractService<IAuthor, IAuthorDto> {
    
  protected repository = authorRepository;
  protected mapper = authorMapper;

}

export const authorService = new AuthorService();
