import { AbstractRepository } from '../common/abstract.repository';
import { IUser, UserModel } from './user.model';

class UserRepository extends AbstractRepository<IUser> {
  protected modelClass = UserModel;

  findByEmailAndPassword(email: string, password: string): Promise<IUser> {
    return this.modelClass.findOne({
      where: {
        email,
        password
      },
      rejectOnEmpty: true
    });
  }
}

export const userRepository = new UserRepository();
