import { AbstractService } from '../common/abstract.service';
import { bookMapper } from './book.mapper';
import { IBook, IBookDto } from './book.model';
import { bookRepository } from './book.repository';

class BookService extends AbstractService<IBook, IBookDto> {
    
  protected repository = bookRepository;
  protected mapper = bookMapper;

}

export const bookService = new BookService();
